import { Component, OnInit } from '@angular/core';
import { EventService } from '../shared/event.service';

import { ToastrService } from 'src/app/common/toastr.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css'],
})
export class EventsListComponent implements OnInit {
  //properties
  events: any;
  //methods

  constructor(
    private eventService: EventService,
    private toastrService: ToastrService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.events = this.route.snapshot.data['events'];
  }
  handleThumbnailClick(eventName: any) {
    this.toastrService.success(eventName);
  }
}
